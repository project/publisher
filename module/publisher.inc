<?php
/**
 * @file
 * @copyright 2008 Sieb Unlimited, LLC
 * @license @see(LICENSE.txt)
 */

require_once 'tools/include/pcommon.inc';
require_once 'tools/lib/publisher.lib';

// vim:ft=php:sts=2:sw=2:ts=2:et:ai:sta:ff=unix
