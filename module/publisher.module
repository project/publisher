<?php
/**
 * @file
 * @copyright 2008 Sieb Unlimited, LLC
 * @license @see(LICENSE.txt)
 */

global $publisherBuild, $publisherBase;
$publisherBuild = '$Id$';
$publisherBase = variable_get('publisher_basedir', dirname(dirname(__FILE__)));

/**
 * @includes
 */
require_once $publisherBase . "/tools/include/publisher.inc";

/**
 * Define publisher user permission controls via hook_perm API
 */
function publisher_perm() {
  return array_merge(array(t('administer publisher')), _publisher_perm());
}

/**
 * Find hooked permission in publisher tool modules.
 *
 * @return array           // The list of publisher permissions.
 */
function _publisher_perm() {
  $ret = array();
  foreach(array_keys(pub_module_registry()) as $module) {
    $ret = array_merge($ret, pub_module_hook_call($module, 'perm'));
  }
  return $ret;
}

function pub_publisher_status() {
  return t('
<p><strong><em>Module Status:</em> This module version is considered 
<em>ALPHA</em> development and not functioning.</strong>  This module version 
should be used for evaluation of development only.  Released versions of this 
module will be announced on the developer and support lists.</p>
  ');
}

function pub_publisher_header($form, $tool = 'Publisher', $module = 'Publisher', $build = NULL) {
  global $publisherBuild;
  if (!isset($build)) {
    $build = $publisherBuild;
  }
  $output = '<p>';
  $output .= '<em>Sponsored by:</em> <a href="http://give-me-an-offer.com" target="_blank">Give-Me-An-Offer.com</a><br/>';
  $output .= '<em>Tool:</em> ' . $tool . '<br/>';
  $output .= '<em>Module:</em> ' . $module . '<br/>';
  $output .= '<em>Form:</em> ' . $form . '<br/>';
  $output .= '<em>Build:</em> ' . $build . '<br/>';
  $output .= '</p>';
  $output .= pub_module_status('publisher');
  return t($output);
}

/**
 * Define publisher help function via hook_help API
 */
function publisher_help($what) {
  switch ($what) {
    case 'admin/help#publisher': {
      require 'help/publisher.help';
    } break;

    default: {
      switch (arg(1)) {
        case 'help':
        case 'publisher': {
          switch (arg(2)) {
            case 'msp': {
              require 'help/publisher.msp.help';
            } break;

            case 'merchants': {
              require 'help/publisher.merchants.help';
            } break;

            case 'webs': {
              require 'help/publisher.webs.help';
            } break;

            default: {
              require 'help/publisher.default.help';
            } break;
          }
        } break;
        default: {
          $output = NULL;
        } break;
      }
    }
  }
  return $output;
}

function publisher_menu($may_cache) {
  static $pub_menu;
  if (!isset($pub_menu)) {
    require 'menu/publisher.menu';
  }

  return $pub_menu->get();
}


function admin_publisher() {
  $admin_tool = arg(2);
  $op = arg(3);
  $output = '';
  switch ($admin_tool) {
    case 'msp': {
      require 'form/publisher.msp.form';
      $output = '<p>' . theme('item_list', array('#message' => print_r($mspForm->get(),true))) . '</p>';
    //  $output = drupal_render($mspForm->id, $mspForm->get());
    } break;
    case 'merchants': {
    } break;
    case 'webs': {
    } break;
    default: {
      $output = t('<p><h3>Available tools:</h3></p>');
      $links = array();
      require 'menu/publisher.menu';
      $links[] = $pub_menu->link('msp');
      $links[] = $pub_menu->link('merchants');
      $links[] = $pub_menu->link('websites');
      $output .= '<p>'. theme('item_list', $links) . '</p>';
    }
  }
  return $output;
}

function admin_publisher_msp_add_form() {
  require 'form/publisher.msp_add.form';
  return $mspForm;
}

function admin_publisher_msp_add_form_submit($formId, $formValues) {
  require 'form/submit/publisher.msp_add.submit';
}

function admin_publisher_msp_add_form_validate($formId, $formValues) {
  require 'form/validate/publisher.msp_add.validate';
}

function theme_admin_publisher_msp_add_form($form) {
  require 'form/publisher.msp_add.form';
  return drupal_render($form);
}

function admin_publisher_msp_list_form() {
  require 'form/publisher.msp_list.form';
  return $mspForm;
}

function admin_publisher_msp_list_form_submit($formId, $formValues) {
  require 'form/submit/publisher.msp_list.submit';
}

function admin_publisher_msp_list_form_validate($formId, $formValues) {
  require 'form/validate/publisher.msp_list.validate';
}

function theme_admin_publisher_msp_list_form($form) {
  require 'form/theme/publisher.msp_list.theme';
  return $theme;
}

function _publisher_msp_get($args) {
  static $sql = 'SELECT * FROM {pub_msp} WHERE id IN (%s)';
  if (is_array($args)) {
    $args = implode(', ', $args);
  }
  $result = db_query($sql, $args);
  $msps = array();
  while ($msp = db_fetch_object($result)) {
    $msps[] = $msp;
  }
  return $msps;
}

function _publisher_msp_enable($args) {
  static $sql = 'UPDATE {pub_msp} SET enabled = 1 WHERE id = %d';
  db_query($sql, $args);
}

function _publisher_msp_disable($args) {
  static $sql = 'UPDATE {pub_msp} SET enabled = 0 WHERE id = %d';
  db_query($sql, $args);
}

function _publisher_msp_delete($args) {
  static $sql = 'UPDATE {pub_msp} SET deleted = 1, enabled = 0 WHERE id = %d';
  db_query($sql, $args);
}

function _publisher_msp_undelete($args) {
  static $sql = 'UPDATE {pub_msp} SET deleted = 0 WHERE id = %d';
  db_query($sql, $args);
}

// vim:ft=php:sts=2:sw=2:ts=2:et:ai:sta:ff=unix
