<?php
/**
 * Include publisher functions
 */

/**
 * @include
 */
if (defined('PUBLISHER_LIB')) {
  require_once PUBLISHER_LIB . '/pfnc.lib';
}
else {
  require_once dirname(__FILE__).'/../lib/pfnc.lib';
}

// vim:ft=php:sts=2:sw=2:ts=2:et:ai:sta:ff=unix
