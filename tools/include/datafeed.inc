<?php
/**
 * @file
 * @copyright 2008 Sieb Unlimited, LLC
 * @license @see(LICENSE.txt)
 */

$thisDir = dirname(__FILE__);
require_once 'pcommon.inc';
require_once 'publisher.inc';
//TODO: require_once "$thisDir/../lib/datafeed.lib";

// vim:ft=php:sts=2:sw=2:ts=2:et:ai:sta:ff=unix
