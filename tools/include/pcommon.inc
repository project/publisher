<?php
/**
 * Publisher common include file.
 * All publisher tools files will include this file first.
 */

/**
 * @const DOCROOT
 *  DOCROOT will be used to search for a config file.
 */
define ('DOCROOT', $_SERVER['DOCUMENT_ROOT']);

/**
 * @include
 */
require_once 'pfnc.inc';

if (@stat(DOCROOT.'/local/etc/publisher.config')) {
  require_once DOCROOT.'/local/etc/publisher.config';
}
elseif (@stat(DOCROOT.'/etc/publisher.config')) {
  require_once DOCROOT.'/etc/publisher.config';
}
elseif (@stat(dirname(dirname(__FILE__)).'/etc/publisher.config')) {
  require_once dirname(dirname(__FILE__)).'/etc/publisher.config';
}

// vim:ft=php:sts=2:sw=2:ts=2:et:ai:sta:ff=unix
