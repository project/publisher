<?php
/**
 * @file
 * @copyright 2008 Sieb Unlimited, LLC
 * @license @see(LICENSE.txt)
 * $Id$
 **/

/**
 * Build the Publisher menu class
 **/
if (!class_exists('pub_menu')) {
  class pub_menu {
    /** 
     * @var array $menu
     * Contains the items of the menu with Drupal requirements.
     */
    var $menu = array();
    var $autoWeight = 0;
    var $currentItem = NULL;

    /**
     * Initialzation
     */
    function pub_menu() {
      $this->menu = array();
    }

    function item($item, $title=NULL, $path=NULL, $desc=NULL) {
      $this->currentItem = $item;
      $this->menu[$item] = array();
      if (isset($title)) {
        $this->title($title);
      }
      if (isset($path)) {
        $this->path($path);
      }
      if (isset($desc)) {
        $this->description($desc);
      }
      $this->type(MENU_NORMAL_ITEM);
      $this->weight($this->autoWeight);
    }

    function access($access) {
      $this->menu[$this->currentItem]['access'] = is_string($access)
                                                ? user_access($access)
                                                : $access
                                                ;
    }

    function callback($func, $func_args=NULL) {
      $this->menu[$this->currentItem]['callback'] = $func;
      $this->type(MENU_CALLBACK);
      $this->callbackArguments($func_args);
    }

    function callbackArguments($args) {
      if (isset($args)) {
        if (!is_array($args)) {
          $args = array($args);
        }
        $this->menu[$this->currentItem]['callback arguments'] = $args;
      }
    }

    function description($desc) {
      $this->menu[$this->currentItem]['description'] = t($desc);
    }

    function path($path) {
      $this->menu[$this->currentItem]['path'] = $path;
    }

    function title($title) {
      $this->menu[$this->currentItem]['title'] = t($title);
    }

    function type($type) {
      $this->menu[$this->currentItem]['type'] = $type;
    }

    function weight($weight) {
      $this->menu[$this->currentItem]['weight'] = $weight;
      $this->autoWeight = ++$weight;
    }

    /**
     * Return the menu array
     */
    function get() {
      return $this->menu;
    }

    /**
     * Return a link for the given menu id or an array of links based on the
     * full set of menu items.
     *
     * @return mixed
     */
    function link($id = NULL) {
      if (isset($id)) {
        return l($this->menu[$id]['title'], 
                 $this->menu[$id]['path'], 
                 array('title' => $this->menu[$id]['description'])
                );
      }
      else {
        $links = array();
        foreach ($this->menu as $id) {
          $links[] = $this->link($id);
        }
        return $links;
      }
    }
  }
}

// vim:ft=php:sts=2:sw=2:ts=2:et:ai:sta:ff=unix
